FROM resin/armv7hf-debian:stretch

RUN [ "cross-build-start" ]

LABEL maintainer="josetruyol@gmail.com" \
      version="V1.0.0" \
      description="Qt 5.7.1 | Debian:Stretch"

ENV RUNTIME_DEPS="build-essential \
    openssl \
    make \
    dbus \
    qt5-default \
    libqt5serialport5 \
    libqt5dbus5"

ENV QT_VERSION="5.7.1"

RUN apt-get update \
    && apt-get install -y --no-install-recommends ${RUNTIME_DEPS} \
    && rm -rf /var/lib/apt/lists/*

COPY test /usr/share/qt-test
# set STOPSGINAL
STOPSIGNAL SIGTERM
COPY infinite_loop.sh /usr/bin/infinite_loop.sh
CMD ["/usr/bin/infinite_loop.sh"]

# stop processing ARM emulation (comment out next line if built on X15)
RUN [ "cross-build-end" ]